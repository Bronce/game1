﻿using UnityEngine;
using System.Collections;
using System.Collections.Specialized;

public class Rotator : MonoBehaviour
{
    public Vector3 RotatingAngle;
    void Update()
    {
        transform.Rotate(RotatingAngle * Time.deltaTime);
    }
}